//
//  AppDelegate.swift
//  CopyNow
//
//  Created by Armifer on 24/04/16.
//  Copyright © 2016 Armifer. All rights reserved.
//

import UIKit
import ReSwift

struct AppReducer: Reducer {
    
    func handleAction(action: Action, state: State?) -> State {
        return State(
            clips: clipReducer(state?.clips, action: action),
            currentClipboardContent: UIPasteboard.generalPasteboard().string!
        )
    }
}

func clipReducer(state: [String]?, action: Action) -> [String] {
    var state = state ?? []
    
    switch action {
    case let action as CreateClip:
        let clip = action.data as! String
        state.append(clip)
        NSUserDefaults.standardUserDefaults().setObject(state, forKey: "CopyNowList")
        return state
    case is LoadClips:
        let clipsFromDisk = NSUserDefaults.standardUserDefaults().arrayForKey("CopyNowList") as? [String] ?? []
        state = clipsFromDisk
        return state
    default:
        return state
    }
}

struct State: StateType {
    var clips: [String]
    var currentClipboardContent: String
}

struct CreateClip: Action {
    let data: Any?
}

struct LoadClips: Action {}

var store = Store<State>(reducer: AppReducer(), state: nil)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        store.dispatch(LoadClips())
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

