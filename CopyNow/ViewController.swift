//
//  ViewController.swift
//  CopyNow
//
//  Created by Armifer on 24/04/16.
//  Copyright © 2016 Armifer. All rights reserved.
//

import UIKit
import ListKit
import ReSwift

class ClipTableViewCell: UITableViewCell, ListKitCellProtocol {
    var model: String? {
        didSet {
            self.textLabel!.text = model ?? ""
        }
    }
}

class ViewController: UIViewController, StoreSubscriber {
    
    @IBAction func newItemFromClipboard() {
        store.dispatch(CreateClip(data: UIPasteboard.generalPasteboard().string))
    }
    
    func newState(state: State) {
        print(state)
        self.dataSource?.array = state.clips
        self.tableView.reloadData()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet var tableView: UITableView!
    
    var dataSource: ArrayDataSource<ClipTableViewCell, String>?
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.dataSource = ArrayDataSource(array: [], cellType: ClipTableViewCell.self)
        tableView.dataSource = dataSource
        tableView.delegate = self
        
        store.subscribe(self)
    }
    
//    override func viewDidAppear(animated: Bool) {
//        self.dataSource?.array = ["WHAT","the","FUCK"]
//        self.tableView.reloadData()
//        super.viewDidAppear(animated)
//    }
}

//MARK: UITableViewDelegate

extension ViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedClip = self.dataSource!.array[indexPath.row]
        print(selectedClip)
    }
    
}

extension ViewController {
    
}